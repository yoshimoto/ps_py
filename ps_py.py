def run():
    import datetime
    import os
    import platform
    date = datetime.datetime.now().strftime("%Y-%m-%d")
    time = datetime.datetime.now().strftime("%H-%M-%S")
    minute = datetime.datetime.now().strftime("%M")
    if not os.path.exists("main_dir.txt"):
        print('Make "main_dir.txt"')
        return -1
    with open("main_dir.txt") as f:
        main_dir = f.readline().strip()
    node_dir = os.path.join(main_dir, platform.node())
    node_file = os.path.join(node_dir, "counter.txt")

    dir = os.path.join(node_dir, date)
    file = os.path.join(dir, time + ".json")

    all = False
    if not os.path.exists(main_dir):
        os.makedirs(main_dir)
        all = True
    if not os.path.exists(main_dir):
        print(f"Cannot make {main_dir}")
        return -1
    if not os.path.exists(dir):
        os.makedirs(dir)
        all = True
    import glob
    buf = glob.glob(os.path.join(dir, "*.json"))
    if len(buf) == 0:
        all = True
    if minute == "00":
        all = True

    os_name = platform.system()
    if os_name == "Windows":
        import ps_windows
        obj_process, obj_process_total = ps_windows.get_process()
        obj_busy_process = {}
        for key, value in obj_process.items():
            if value["CpuPercent"] < 0.1:
                continue
            if key == "_total":
                continue
            if key == "idle":
                continue
            obj_busy_process[key] = value
        obj_physicaldisk, obj_logicaldisk = ps_windows.get_disk()
    else:
        import ps_ubuntu
        obj_process = {}
        ps_ubuntu.get_process_name(obj_process)
        ps_ubuntu.get_process(obj_process)
        ps_ubuntu.get_disk(obj_process)
        ps_ubuntu.get_memory(obj_process)
        ps_ubuntu.complement_keys(obj_process)
        obj_process_total = None
        obj_busy_process = {}
        for key, value in obj_process.items():
            if value["CpuPercent"] < 0.01 and value["MemoryWorking"] < 1024:
                continue
            obj_busy_process[key] = value
        obj_physicaldisk, obj_logicaldisk = None, None

    # Output
    obj = {}
    if all:
        obj["Os"] = platform.system()
        obj["OsRelease"] = platform.release()
        obj["OsVersion"] = platform.version()
        obj["MachineType"] = platform.machine()
        obj["Processor"] = platform.processor()
        obj["MachineName"] = platform.node()
        import multiprocessing
        obj["Cores"] = multiprocessing.cpu_count()

    obj["Process"] = obj_busy_process
    obj["ProcessTotal"] = obj_process_total

    if all:
        obj["PhysicalDisk"] = obj_physicaldisk
        obj["LogicalDisk"] = obj_logicaldisk
        if os_name == "Windows":
            if any([obj["Operation"] != "OK" for obj in obj_physicaldisk]) or any([obj["Health"] != "Healthy" for obj in obj_physicaldisk]):
                import ps_notification
                ps_notification.run("Physical Disk Error on {}".format(platform.node()), "\n".join(
                    [str(obj) for obj in obj_physicaldisk]))
            if any([obj["Health"] != "Healthy" for obj in obj_logicaldisk]):
                import ps_notification
                ps_notification.run("Logical Disk Error on {}".format(platform.node()), "\n".join(
                    [str(obj) for obj in obj_logicaldisk]))

    import json
    json.dump(obj, open(file, "w"), indent=1)

    if os.path.exists(node_file):
        with open(node_file) as f:
            n_recorded = int(f.readline())
    else:
        n_recorded = 0
    if n_recorded >= 60:
        # Every 60 times (every 1 hour)
        os.chdir(main_dir)
        import git
        try:
            repo = git.Repo()
            # Pull
            origin = repo.remotes.origin
            origin.pull()
            # Commit
            repo.git.add("*")
            repo.git.commit('.', '-m', f"{platform.node()} {date} {time}")
            # Push
            origin = repo.remote(name='origin')
            origin.push()
        except:
            pass
        n_recorded = 0
    else:
        n_recorded += 1
    with open(node_file, "w") as f:
        f.write(str(n_recorded))

    return 0


if __name__ == "__main__":
    ret = run()
    import sys
    sys.exit(ret)
