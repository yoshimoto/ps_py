# README
## Windows
```
gitpythonのインストール
pip install gitpython

ホームディレクトリ または ログを保存したい場所へ移動
cd %HOMEDRIVE%%HOMEPATH%

Push権限を持ったアカウントでcloneする
git clone https://***.***.git .ps_py

このプロジェクトをclone
https://gitlab.com/yoshimoto/ps_py

ホームディレクトリ以外、又は ログを保存したい場所をmain_dir.txt へ記述

通知を有効にする場合は、py_notification.txtへ以下のように記述
  slack https://hooks.slack.com/services/**/**/****
  又は
  gitlab private_token project_id
  通知をテストするにはpy_notification.py を実行する
  

タスクスケジューラから以下のように設定する
  トリガーの編集 設定で「毎日」「間隔1日」「開始をX 0:00:00」、詳細設定で「繰り返し間隔1分」「継続時間1日間」
  プログラム「pythonw.exeの完全パス」引数の追加「ps_py.py」開始「main_dir.txtを置いたディレクトリ」
```

## Ubuntu
```sh
# install sysstat
sudo apt install sysstat

# git setting if you need
git config --global user.email "your email"
git config --global user.name "your name"
git config --global credential.helper store

# install python3-pip if you need
sudo apt install python3-pip
pip3 install gitpython

# clone ps_py
cd
git clone https://gitlab.com/yoshimoto/ps_py
pico main_dir.txt
#write as /home/{user}/.ps_py/
cd
git clone https://your_repository .ps_py

# Test run of the script
python3 /home/{user}/ps_py/ps_py.py
cd ~/.ps_py/
# and make sure that the {HOSTNAME} directory has been created

# Test run of git push
cd ~/.ps_py
git pull
git add *
git commit -m "{HOSTNAME} commit"
git push

# CRON setting
sudo pico /etc/cron.d/ps_py

# write as */2 * * * * {user} python3 /home/{user}/ps_py/ps_py.py

sudo service cron restart
sudo pico /etc/rsyslog.d/50-default.conf

# un commentout cron
#-#cron.* /var/log/cron.log
#+cron.* /var/log/cron.log

sudo service rsyslog restart
service cron status
```