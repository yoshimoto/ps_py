CpuPercent = r"% Processor Time"
CpuUserPercent = r"% User Time"
CpuKernelPercent = r"% Privileged Time"
MemoryWorking = r"Working Set"
MemoryVirtual = r"Virtual Bytes"
PageFaults = r"Page Faults/sec"
IoRead = r"IO Read Bytes/sec"
IoWrite = r"IO Write Bytes/sec"


def ps_run1(command):
    import subprocess
    command = f'powershell -Command {command} | Out-file -filepath ps_windows.txt -Encoding UTF8'
    print(command)
    subprocess.run(command)
    buf = []
    with open("ps_windows.txt", encoding="utf-8") as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            if len(line) > 0:
                buf.append(line)
    return buf


def ps_run_physicaldisk():
    import subprocess
    command = 'powershell -Command Get-PhysicalDisk | Export-Csv -path ps_windows.txt -Encoding UTF8'
    print(command)
    subprocess.run(command)
    buf = []
    keys_ps = ["DeviceId", "FriendlyName", "SerialNumber", "MediaType",
               "OperationalStatus", "HealthStatus", "AllocatedSize", "Size"]
    keys = ["DeviceId", "Name", "SerialNumber", "MediaType",
            "Operation", "Health", "Allocated", "Capacity"]
    indexs = []
    with open("ps_windows.txt", encoding="utf-8") as f:
        lines = f.readlines()
        strs = lines[1].split(',')
        for k0 in keys_ps:
            indexs.append([i for i, k1 in enumerate(strs) if k0 in k1][0])
        for line in lines[2:]:
            strs = line.split(',')
            values = [strs[i].replace('"', '') for i in indexs]
            buf2 = {}
            for key, value in zip(keys, values):
                buf2[key] = value
            buf.append(buf2)
    return buf


def ps_run_logicaldisk():
    import subprocess
    command = 'powershell -Command Get-Disk | Export-Csv -path ps_windows.txt -Encoding UTF8'
    print(command)
    subprocess.run(command)
    buf = []
    keys_ps = ["Number", "FriendlyName",
               "HealthStatus", "AllocatedSize", "Size"]
    keys = ["Number", "Name", "Health", "Allocated", "Capacity"]
    indexs = []
    with open("ps_windows.txt", encoding="utf-8") as f:
        lines = f.readlines()
        strs = lines[1].split(',')
        for k0 in keys_ps:
            indexs.append([i for i, k1 in enumerate(strs) if k0 in k1][0])
        for line in lines[2:]:
            strs = line.split(',')
            values = [strs[i].replace('"', '') for i in indexs]
            buf2 = {}
            for key, value in zip(keys, values):
                buf2[key] = value
            buf.append(buf2)
    return buf


def read_ps(ret, ps_keys, keys):
    i0 = -1
    ps_keys = [l.lower() for l in list(ps_keys)]
    obj = {}
    i1 = 0
    while i1 < len(ret):
        flag = False
        for ps_key, key in zip(ps_keys, keys):
            if ps_key in ret[i1]:
                strs = ret[i1].split("\\")
                name = strs[3][8:-1]
                if len(name) > 40: # 長いprocess名で改行によるバグが起こるため
                    continue
                if name not in obj:
                    obj[name] = {}
                i1 += 1
                obj[name][key] = float(ret[i1])
                i1 += 1
                flag = True
                break
        if flag:
            continue
        i1 += 1

    return obj


def get_process():

    command = (r"Get-Counter '\Process(*)\{0[0]}',"
               r"'\Process(*)\{0[1]}',"
               r"'\Process(*)\{0[2]}',"
               r"'\Process(*)\{0[3]}',"
               r"'\Process(*)\{0[4]}',"
               r"'\Process(*)\{0[5]}',"
               r"'\Process(*)\{0[6]}',"
               r"'\Process(*)\{0[7]}'")
    keys = ["CpuPercent", "CpuUserPercent", "CpuKernelPercent",
            "MemoryWorking", "MemoryVirtual", "PageFaults", "IoRead", "IoWrite"]
    ps_keys = [eval(key) for key in keys]
    ret = ps_run1(command.format(ps_keys))
    obj = read_ps(ret, ps_keys, keys)
    return obj, obj["_total"]


def get_disk():
    return ps_run_physicaldisk(), ps_run_logicaldisk()


if __name__ == "__main__":
    print(get_process())
    print(get_disk())
