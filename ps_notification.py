import os
import sys


def post_slack(url, title, text):
    # pip install slackweb
    import slackweb
    # get URL for Incoming WebHooks in https://***.slack.com/apps
    slack = slackweb.Slack(url=url)
    slack.notify(text='*{0}*\n{1}'.format(title, text))


def post_gitlab(private_token, project_id, title, text):
    # pip install python-gitlab
    import gitlab
    # make private_token for api at https://gitlab.com/profile/personal_access_tokens
    gl = gitlab.Gitlab('https://gitlab.com/', private_token=private_token)
    project = gl.projects.get(project_id)
    project.issues.create({'title': title, 'description': text})


def run(title, message):
    if not os.path.exists("ps_notification.txt"):
        print("ps_notification.txt is not found.")
        return 1
    strs = open("ps_notification.txt").readline().strip().split()
    try:
        if strs[0] == "slack":
            post_slack(strs[1], title, message)
        elif strs[0] == "gitlab":
            post_gitlab(strs[1], strs[2], title, message)
        else:
            return 1
    except:
        print("Error", sys.exc_info())
        return 1
    return 0


if __name__ == "__main__":
    ret = run("Title(test)", "Message(test)")
    sys.exit(ret)
