import glob
import os
from datetime import datetime as dt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import matplotlib
import json


def get_dir_hostname(repo_dir):
    dirnames = glob.glob(f"{repo_dir}/*/")
    hostnames = [os.path.basename(os.path.dirname(
        dir)) for dir in dirnames if os.path.basename(os.path.dirname(dir)) != "graph"]
    return dirnames, hostnames


def get_dates(dirname):
    datedirs = glob.glob(f"{dirname}/*/")
    datestrs = [os.path.basename(os.path.dirname(dir)) for dir in datedirs]
    dates = []
    for datestr in datestrs:
        tdatetime = dt.strptime(datestr, '%Y-%m-%d')
        dates.append(tdatetime)
    return datedirs, dates


def get_times(datedir, date):
    timefiles = glob.glob(f"{datedir}/*.json")
    timestrs = [os.path.splitext(os.path.basename(file))[0]
                for file in timefiles]
    times = []
    for timestr in timestrs:
        tdatetime = dt.strptime(timestr, '%H-%M-%S')
        times.append(tdatetime)
    return timefiles, times


def plot_each_process(target_path, datedirs, dates):

    year = 2000
    month = 1
    pdf = None
    txt = None
    for datedir, date in zip(datedirs, dates):
        if year != date.year or month != date.month:
            year = date.year
            month = date.month
            if pdf != None:
                pdf.close()
                txt.close()
                print("PDF close")
            pdf = PdfPages(f"{target_path}_{year}-{month:02d}.pdf")
            txt = open(f"{target_path}_{year}-{month:02d}.txt", "w")
            print("PDF open")

        ProcessNames = []
        ProcessNamesSimple = []
        DateTimes = []
        NProcesses = []
        CpuPercents = []
        MemoryWorkings = []
        TotalCpuPercents = []
        TotalMemoryWorkings = []
        timefiles, times = get_times(datedir, date)

        Os = ""
        MemoryUnit = 1
        for timefile, time in zip(timefiles, times):
            obj = json.load(open(timefile))
            if Os == "":
                Os = obj["Os"]
                if Os == "Linux":
                    MemoryUnit = 1000
            TotalCpuPercents.append(0)
            TotalMemoryWorkings.append(0)
            LocalProcessNames = []
            for process_name in obj["Process"]:
                if obj["Process"][process_name]["CpuPercent"] < 50:
                    continue
                if "MemoryWorking" not in obj["Process"][process_name]:
                    continue
                if process_name.count("/") > 0:
                    simple_process_name = process_name[0:process_name.rfind(
                        "/")]
                elif process_name.count("#") > 0:
                    simple_process_name = process_name[0:process_name.rfind(
                        "#")]
                elif process_name == "_total" or process_name == "idle":
                    continue
                else:
                    simple_process_name = process_name
                LocalProcessNames.append(simple_process_name)
                nprocess = LocalProcessNames.count(simple_process_name)
                NProcesses.append(nprocess)
                ProcessNames.append(process_name)
                ProcessNamesSimple.append(simple_process_name)
                CpuPercent = obj["Process"][process_name]["CpuPercent"]
                MemoryWorking = obj["Process"][process_name]["MemoryWorking"]
                CpuPercents.append(CpuPercent)
                MemoryWorkings.append(MemoryWorking)
                TotalCpuPercents[-1] += CpuPercent
                TotalMemoryWorkings[-1] += MemoryWorking / \
                    1000_000_000*MemoryUnit
                DateTimes.append(time)

        if len(ProcessNames) == 0:
            continue

        ProcessNamesSimple, DateTimes, NProcesses, CpuPercents, MemoryWorkings = zip(
            *sorted(sorted(zip(ProcessNamesSimple, DateTimes, NProcesses, CpuPercents, MemoryWorkings), key=lambda d: d[0])))
        ProcessNamesSimple = list(ProcessNamesSimple)
        DateTimes = list(DateTimes)
        NProcesses = list(NProcesses)

        for a, b, c, d, e in zip(DateTimes, ProcessNamesSimple, NProcesses, CpuPercents, MemoryWorkings):
            txt.write(
                f"{date.date()} {a.time()} {b} {c} {d:.1f} {e/1000_000_000*MemoryUnit:.3f}\n")

        DateTimes += [dt.strptime("00-00-00", '%H-%M-%S'),
                      dt.strptime("23-59-59", '%H-%M-%S')]
        ProcessNamesSimple += ["0", "0"]
        NProcesses += [1, 10]

        process_height = len(list(set(ProcessNamesSimple)))*0.1+2

        fig, axes = plt.subplots(3, 1, figsize=(15, process_height+3*2),
                                 gridspec_kw={'height_ratios': [3, 3, process_height]})

        axes[0].set_title(date.date())
        axes[0].plot(times, TotalCpuPercents)
        axes[0].set_ylabel("CPU [%]")
        axes[0].set_ylim(0, None)
        axes[1].plot(times, TotalMemoryWorkings)
        axes[1].set_ylim(0, None)
        axes[1].set_ylabel("Memory working [GB]")
        axes[2].scatter(DateTimes, ProcessNamesSimple,
                        c=NProcesses, marker="|", cmap="tab10")
        for ax in axes:
            ax.xaxis.set_major_formatter(
                matplotlib.dates.DateFormatter('%H:%M'))

        pdf.savefig()
        plt.close()
    pdf.close()
    txt.close()
    print("PDF close")


def plot_processes(repo_dir, graph_dir):
    dirnames, hostnames = get_dir_hostname(repo_dir)
    for dirname, hostname in zip(dirnames, hostnames):
        datedirs, dates = get_dates(dirname)
        plot_each_process(os.path.join(graph_dir, hostname), datedirs, dates)
