
def ps_run_shell(command):
    import subprocess
    print(command)
    with open("ps_ubuntu.txt", "w") as f:
        subprocess.run(command, shell=True, stdout=f)
    buf = []
    with open("ps_ubuntu.txt", encoding="utf-8") as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            if len(line) > 0:
                buf.append(line)
    return buf


def get_pids(processes):
    pids = ""
    for process_name in processes:
        if pids == "":
            pids += process_name.split('/')[-1]
        else:
            pids += "," + process_name.split('/')[-1]
    return pids


def get_process_name(processes):
    ret = ps_run_shell("pidstat")
    for line in ret[2:]:
        line = line.strip()
        strs = line.split()
        process_name = strs[9] + "/" + strs[2]
        if process_name not in processes:
            processes[process_name] = {}
    return processes


def get_process(processes):
    if len(processes) == 0:
        return processes
    ret = ps_run_shell(f"pidstat -u 1 1 -p {get_pids(processes)}")
    for line in ret[2:]:
        line = line.strip()
        strs = line.split()
        if strs[9] == "Command":
            break
        process_name = strs[9] + "/" + strs[2]
        if process_name not in processes:
            processes[process_name] = {}
        processes[process_name]["CpuPercent"] = float(strs[7])
        processes[process_name]["CpuUserPercent"] = float(strs[3])
        processes[process_name]["CpuKernelPercent"] = float(strs[4])
        processes[process_name]["CpuGuestPercent"] = float(strs[5])
        processes[process_name]["CpuWaitPercent"] = float(strs[6])
    return processes


def get_disk(processes):
    if len(processes) == 0:
        return processes
    ret = ps_run_shell(f"pidstat -d 1 1 -p {get_pids(processes)}")
    for line in ret[2:]:
        line = line.strip()
        strs = line.split()
        if strs[7] == "Command":
            break
        process_name = strs[7] + "/" + strs[2]
        if process_name not in processes:
            processes[process_name] = {}
        processes[process_name]["IoRead"] = float(strs[3])
        processes[process_name]["IoWrite"] = float(strs[4])
    return processes


def get_memory(processes):
    if len(processes) == 0:
        return processes
    ret = ps_run_shell(f"pidstat -r 1 1 -p {get_pids(processes)}")
    for line in ret[2:]:
        line = line.strip()
        strs = line.split()
        if strs[8] == "Command":
            break
        process_name = strs[8] + "/" + strs[2]
        if process_name not in processes:
            processes[process_name] = {}
        processes[process_name]["PageFaults"] = float(strs[3])
        processes[process_name]["MemoryVirtual"] = float(strs[5])
        processes[process_name]["MemoryWorking"] = float(strs[6])
    return processes


def complement_keys(processes):
    keys = ["CpuPercent", "CpuUserPercent", "CpuKernelPercent",
            "CpuGuestPercent", "CpuWaitPercent"]
    keys += ["IoRead", "IoWrite"]
    keys += ["PageFaults", "MemoryVirtual", "MemoryWorking"]
    for process, value in processes.items():
        for key in keys:
            if key not in processes[process]:
                processes[process][key] = 0


if __name__ == "__main__":
    processes = {}
    get_process(processes)
    get_disk(processes)
    for process, value in processes.items():
        print(process, value)
